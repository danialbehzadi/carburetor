# Part of Carburetor project
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2019-2024.

"""
Main module for carburetor
"""

import sys

from gi.repository import Adw, Gio

from . import actions, ui


class Application(Adw.Application):
    """
    main window of carburetor
    """

    def __init__(self, version):
        super().__init__(
            application_id="io.frama.tractor.carburetor",
            flags=Gio.ApplicationFlags.DEFAULT_FLAGS,
        )
        self.version = version
        self.window = None
        self.prefs = None
        self.about = None
        self.shortcuts = None

    def do_startup(self, *args, **kwargs) -> None:
        Adw.Application.do_startup(self)
        actions.do_startup(self)

    def do_activate(self, *args, **kwargs) -> None:
        if not self.window:
            ui.css()
            window = ui.get("MainWindow")
            self.add_window(window)
            self.window = window
        self.window.present()


def main(version=None) -> None:
    """
    The application's entry point.
    """
    app = Application(version)
    return app.run(sys.argv)
