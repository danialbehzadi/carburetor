# Carburetor: anonymous browsing made easy
Discover anonymous browsing with Carburetor on your phones and computers. Tailored for GNOME, it's your hidden superhero for online adventures, powered by TOR. Don't get your hands dirty with system files any more – just tap in the app, keeping your online world safe and private. Customize settings if you want, but you don't have to. Carburetor is Free Software and puts you in control. No worries, just enjoy your anonymous browsing!

![Main window of Carburetor](https://tractor.frama.io/images/carburetor-main.png)

Carburetor complies with the [GNOME Code of Conduct](https://conduct.gnome.org).

## Install
Carburetor is natively packaged for some distros:

[![Packaging status](https://repology.org/badge/vertical-allrepos/carburetor.svg)](https://repology.org/project/carburetor/versions)

If you're on any other distribution or OS, Tractor team welcomes you to make build recepie of yours.

You can also get it from [Flathub](https://flathub.org/apps/io.frama.tractor.carburetor).

## Run
you can run `carburetor` by command line or through your desktop environment.

## Contribute
Contributing is always appreciated. See [CONTRIBUTING](https://framagit.org/tractor/carburetor/-/blob/main/CONTRIBUTING.md) for more information.
